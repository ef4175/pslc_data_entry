from tkinter import *
import csv
import time

macHeader = '  {0:40s} {1:36s}  {2:77s}  {3:19s} {4:27s} {5:27s} {6:14s} {7:32s} {8:30s}'
visHeader = '  {0:42s} {1:40s}  {2:82s}  {3:21s} {4:30s} {5:30s} {6:15s} {7:34s} {8:30s}'
sevHeader = '  {0:48s} {1:45s}  {2:95s}  {3:24s} {4:33s} {5:34s} {6:18s} {7:38s} {8:30s}'
headerList = ['Time In', 'Time Out', 'E-mail', 'Major', 'Last Name', 'First Name', 'Course', 'Tutor', 'Book', 'Staff Initials']
changedMajor = False
changedCourse = False
allStudents = {}
emailAddress = ''
major = ''
lastName = ''
firstName = ''
course = ''
majorID = 0
courseID = 0
tutorName = ''
bookName = ''
timeIn = ''
timeOut = ''
initials = ''
fullInfo = []
mainFrameWidth = 1500
mainFrameHeight = 600
mainCanvasWidth = 1500
mainCanvasHeight = 600
scrollOne = 0
scrollTwo = 0
timeLabelsWidth = 23
emailLabelWidth = 46
majorLabelWidth = 12
nameLabelsWidth = 18
courseLabelWidth = 10
tutorLabelWidth = 18
bookLabelWidth = 10
yPosition = 5
xPosition = 5
addWindowNotOpen = True
modWindowNotOpen = True
checkWindowNotOpen = True
clearWindowNotOpen = True

def scrollCanvas(event):
    global mainCanvas
    mainCanvas.yview_scroll(int(-1*(event.delta/120)), "units")

root = Tk()

with open ("studentdata.csv", "a") as file:
    output = csv.writer(file, lineterminator = '\n')
    output.writerow(headerList)

class getInfo(Frame):
    def __init__(self, parent, action):
        Frame.__init__(self, parent)
        
        self.emailFrame = Frame(self)
        self.emailLabel = Label(self.emailFrame)
        self.emailInput = Entry(self.emailFrame)
        self.emailLabel['text'] = 'E-mail address'
        self.emailLabel.pack(side = LEFT)
        self.emailInput.pack(side = LEFT, fill = X, expand = YES)
        self.emailFrame.pack(fill = X)

        self.majorFrame = Frame(self)
        self.buffer1 = Label(self)
        self.buffer1.pack()
        self.majorLabel = Label(self.majorFrame)
        self.majorLabel['text'] = 'Major'
        self.majorLabel.pack(anchor = NW)
        self.biochemistryMajor = Button(self.majorFrame, text = 'Biochemistry', command = pickBiochemistry)
        self.biochemistryMajor.pack(side = LEFT)
        self.biologyMajor = Button(self.majorFrame, text = 'Biology', command = pickBiology)
        self.biologyMajor.pack(side = LEFT)
        self.chemistryMajor = Button(self.majorFrame, text = 'Chemistry', command = pickChemistry)
        self.chemistryMajor.pack(side = LEFT)
        self.nursingMajor = Button(self.majorFrame, text = 'Nursing', command = pickNursing)
        self.nursingMajor.pack(side = LEFT)
        self.pbMajor = Button(self.majorFrame, text = 'Post-Bacc.', command = pickPb)
        self.pbMajor.pack(side = LEFT)
        self.psychologyMajor = Button(self.majorFrame, text = 'Psychology', command = pickPsychology)
        self.psychologyMajor.pack(side = LEFT)
        self.undeclaredMajor = Button(self.majorFrame, text = 'Undeclared', command = pickUndeclared)
        self.undeclaredMajor.pack(side = LEFT)
        self.otherMajorLabel = Label(self.majorFrame)
        self.otherMajorLabel['text'] = 'Other major'
        self.otherMajorInput = Entry(self.majorFrame)
        self.otherMajorInput.pack(side = RIGHT, fill = X, expand = YES)
        self.otherMajorLabel.pack(side = RIGHT)
        self.majorFrame.pack(fill = X)

        self.lastNameFrame = Frame(self)
        self.buffer2 = Label(self)
        self.buffer2.pack()
        self.lastNameLabel = Label(self.lastNameFrame)
        self.lastNameLabel['text'] = 'Last name'
        self.lastNameLabel.pack(side = LEFT)
        self.lastNameInput = Entry(self.lastNameFrame)
        self.lastNameInput.pack(side = LEFT, fill = X, expand = YES)
        self.lastNameFrame.pack(fill = X)

        self.firstNameFrame = Frame(self)
        self.firstNameLabel = Label(self.firstNameFrame)
        self.firstNameLabel['text'] = 'First name'
        self.firstNameLabel.pack(side = LEFT)
        self.firstNameInput = Entry(self.firstNameFrame)
        self.firstNameInput.pack(side = LEFT, fill = X, expand = YES)
        self.firstNameFrame.pack(fill = X)

        self.buffer3 = Label(self)
        self.buffer3.pack()

        self.courseLabel = Label(self)
        self.courseLabel['text'] = 'Course'
        self.courseLabel.pack(anchor = NW)

        self.chemistryCoursesFrame = Frame(self)
        self.chem100 = Button(self.chemistryCoursesFrame, text = 'CHEM 100', command = pickChem100)
        self.chem100.pack(side = LEFT)
        self.chem101 = Button(self.chemistryCoursesFrame, text = 'CHEM 101', command = pickChem101)
        self.chem101.pack(side = LEFT)
        self.chem102 = Button(self.chemistryCoursesFrame, text = 'CHEM 102', command = pickChem102)
        self.chem102.pack(side = LEFT)
        self.chem104 = Button(self.chemistryCoursesFrame, text = 'CHEM 104', command = pickChem104)
        self.chem104.pack(side = LEFT)
        self.chem120 = Button(self.chemistryCoursesFrame, text = 'CHEM 120', command = pickChem120)
        self.chem120.pack(side = LEFT)
        self.chem222 = Button(self.chemistryCoursesFrame, text = 'CHEM 222', command = pickChem222)
        self.chem222.pack(side = LEFT)
        self.chem224 = Button(self.chemistryCoursesFrame, text = 'CHEM 224', command = pickChem224)
        self.chem224.pack(side = LEFT)
        self.chemistryCoursesFrame.pack(fill = X)

        self.physicsCoursesFrame = Frame(self)
        self.phys100 = Button(self.physicsCoursesFrame, text = 'PHYS 100', command = pickPhys100)
        self.phys100.pack(side = LEFT)
        self.phys101 = Button(self.physicsCoursesFrame, text = 'PHYS 101', command = pickPhys101)
        self.phys101.pack(side = LEFT)
        self.phys110 = Button(self.physicsCoursesFrame, text = 'PHYS 110', command = pickPhys110)
        self.phys110.pack(side = LEFT)
        self.phys111 = Button(self.physicsCoursesFrame, text = 'PHYS 111', command = pickPhys111)
        self.phys111.pack(side = LEFT)
        self.phys120 = Button(self.physicsCoursesFrame, text = 'PHYS 120', command = pickPhys120)
        self.phys120.pack(side = LEFT)
        self.phys121 = Button(self.physicsCoursesFrame, text = 'PHYS 121', command = pickPhys121)
        self.phys121.pack(side = LEFT)
        self.otherCourseLabel = Label(self.physicsCoursesFrame)
        self.otherCourseLabel['text' ] = '          Other course (enter full name)'
        self.otherCourseLabel.pack(side = LEFT)
        self.otherCourseInput = Entry(self.physicsCoursesFrame)
        self.otherCourseInput.pack(side = LEFT, fill = X, expand = YES)
        self.physicsCoursesFrame.pack(fill = X)

        self.buffer4 = Label(self)
        self.buffer4.pack()

        self.tutorFrame = Frame(self)
        self.tutorLabel = Label(self.tutorFrame)
        self.tutorLabel['text'] = 'Tutor'
        self.tutorLabel.pack(side = LEFT)
        self.tutorInput = Entry(self.tutorFrame)
        self.tutorInput.pack(side = LEFT, fill = X, expand = YES)
        self.tutorFrame.pack(fill = X)

        self.buffer5 = Label(self)
        self.buffer5.pack()

        self.bookFrame = Frame(self)
        self.bookLabel = Label(self.bookFrame)
        self.bookLabel['text'] = 'Book'
        self.bookLabel.pack(side = LEFT)
        self.bookInput = Entry(self.bookFrame)
        self.bookInput.pack(side = LEFT, fill = X, expand = YES)
        self.bookFrame.pack(fill = X)

        self.buffer6 = Label(self)
        self.buffer6.pack()

        self.initialFrame = Frame(self)
        self.initialLabel = Label(self.initialFrame)
        self.initialLabel['text' ] = 'Your Initials'
        self.initialLabel.pack(side = LEFT)
        self.initialInput = Entry(self.initialFrame)
        self.initialInput.pack(side = LEFT, fill = X, expand = YES)
        self.initialFrame.pack(fill = X)

        self.buffer7 = Label(self)
        self.buffer7.pack()

        self.addStudentFrame = Frame(self)
        self.addStudentButton = Button(self.addStudentFrame,text = 'Add student',
                                       bg = 'light green', command = addStudent)
        self.addStudentButton.pack(fill = X, expand = YES, side = BOTTOM)
        self.addStudentFrame.pack(fill = X, expand = YES, side = BOTTOM)

class modWindow(Frame):
    global allStudents, course, major, courseID, majorID, changedMajor, changedCourse
    def __init__(self, parent, action, ti):
        Frame.__init__(self, parent)

        self.timeInFrame = Frame(self)
        self.timeInLabel = Label(self.timeInFrame)
        self.timeInInput = Entry(self.timeInFrame)
        self.timeInInput.insert(0, allStudents[ti][0])
        self.timeInLabel['text'] = 'Time In'
        self.timeInLabel.pack(side = LEFT)
        self.timeInInput.pack(side = LEFT, fill = X, expand = YES)
        self.timeInFrame.pack(fill = X)

        self.timeOutFrame = Frame(self)
        self.timeOutLabel = Label(self.timeOutFrame)
        self.timeOutInput = Entry(self.timeOutFrame)
        self.timeOutInput.insert(0, allStudents[ti][1])
        self.timeOutLabel['text'] = 'Time Out'
        self.timeOutLabel.pack(side = LEFT)
        self.timeOutInput.pack(side = LEFT, fill = X, expand = YES)
        self.timeOutFrame.pack(fill = X)

        self.buffer0 = Label(self)
        self.buffer0.pack()

        self.emailFrame = Frame(self)
        self.emailLabel = Label(self.emailFrame)
        self.emailInput = Entry(self.emailFrame)
        self.emailInput.insert(0, allStudents[ti][2])
        self.emailLabel['text'] = 'E-mail address'
        self.emailLabel.pack(side = LEFT)
        self.emailInput.pack(side = LEFT, fill = X, expand = YES)
        self.emailFrame.pack(fill = X)

        changedMajor = False
        self.majorFrame = Frame(self)
        self.buffer1 = Label(self)
        self.buffer1.pack()
        self.majorLabel = Label(self.majorFrame)
        self.majorLabel['text'] = 'Major'
        self.majorLabel.pack(anchor = NW)
        self.biochemistryMajor = Button(self.majorFrame, text = 'Biochemistry', command = lambda: changeBiochemistry(ti))
        if allStudents[ti][3] == 'Biochemistry':
            self.biochemistryMajor['bg'] = 'light blue'
        self.biochemistryMajor.pack(side = LEFT)
        self.biologyMajor = Button(self.majorFrame, text = 'Biology', command = lambda: changeBiology(ti))
        if allStudents[ti][3] == 'Biology':
            self.biologyMajor['bg'] = 'light blue'
        self.biologyMajor.pack(side = LEFT)
        self.chemistryMajor = Button(self.majorFrame, text = 'Chemistry', command = lambda: changeChemistry(ti))
        if allStudents[ti][3] == 'Chemistry':
            self.chemistryMajor['bg'] = 'light blue'
        self.chemistryMajor.pack(side = LEFT)
        self.nursingMajor = Button(self.majorFrame, text = 'Nursing', command = lambda: changeNursing(ti))
        if allStudents[ti][3] == 'Nursing':
            self.nursingMajor['bg'] = 'light blue'
        self.nursingMajor.pack(side = LEFT)
        self.pbMajor = Button(self.majorFrame, text = 'Post-Bacc.', command = lambda: changePb(ti))
        if allStudents[ti][3] == 'Post-Bacc.':
            self.pbMajor['bg'] = 'light blue'
        self.pbMajor.pack(side = LEFT)
        self.psychologyMajor = Button(self.majorFrame, text = 'Psychology', command = lambda: changePsychology(ti))
        if allStudents[ti][3] == 'Psychology':
            self.psychologyMajor['bg'] = 'light blue'
        self.psychologyMajor.pack(side = LEFT)
        self.undeclaredMajor = Button(self.majorFrame, text = 'Undeclared', command = lambda: changeUndeclared(ti))
        if allStudents[ti][3] == 'Undeclared':
            self.undeclaredMajor['bg'] = 'light blue'
        self.undeclaredMajor.pack(side = LEFT)
        self.otherMajorLabel = Label(self.majorFrame) 
        self.otherMajorLabel['text'] = 'Other major'
        self.otherMajorInput = Entry(self.majorFrame)
        if not (allStudents[ti][3] == 'Biochemistry' or allStudents[ti][3]== 'Biology' or allStudents[ti][3] == 'Chemistry'
                or allStudents[ti][3] == 'Nursing' or allStudents[ti][3] == 'Post-Bacc.' or allStudents[ti][3] == 'Psychology'
                or allStudents[ti][3] == 'Undeclared'):
            self.otherMajorInput.insert(0, allStudents[ti][3])
        self.otherMajorInput.pack(side = RIGHT, fill = X, expand = YES)
        self.otherMajorLabel.pack(side = RIGHT)
        self.majorFrame.pack(fill = X)

        self.lastNameFrame = Frame(self)
        self.buffer2 = Label(self)
        self.buffer2.pack()
        self.lastNameLabel = Label(self.lastNameFrame)
        self.lastNameLabel['text'] = 'Last name'
        self.lastNameLabel.pack(side = LEFT)
        self.lastNameInput = Entry(self.lastNameFrame)
        self.lastNameInput.insert(0, allStudents[ti][4])
        self.lastNameInput.pack(side = LEFT, fill = X, expand = YES)
        self.lastNameFrame.pack(fill = X)

        self.firstNameFrame = Frame(self)
        self.firstNameLabel = Label(self.firstNameFrame)
        self.firstNameLabel['text'] = 'First name'
        self.firstNameLabel.pack(side = LEFT)
        self.firstNameInput = Entry(self.firstNameFrame)
        self.firstNameInput.insert(0, allStudents[ti][5])
        self.firstNameInput.pack(side = LEFT, fill = X, expand = YES)
        self.firstNameFrame.pack(fill = X)

        self.buffer3 = Label(self)
        self.buffer3.pack()

        self.courseLabel = Label(self)
        self.courseLabel['text'] = 'Course'
        self.courseLabel.pack(anchor = NW)

        changedCourse = False
        self.chemistryCoursesFrame = Frame(self)
        self.chem100 = Button(self.chemistryCoursesFrame, text = 'CHEM 100', command = lambda: changeChem100(ti))
        if allStudents[ti][6] == 'CHEM 100':
            self.chem100['bg'] = 'light blue'
        self.chem100.pack(side = LEFT)
        self.chem101 = Button(self.chemistryCoursesFrame, text = 'CHEM 101', command = lambda: changeChem101(ti))
        if allStudents[ti][6] == 'CHEM 101':
            self.chem101['bg'] = 'light blue'
        self.chem101.pack(side = LEFT)
        self.chem102 = Button(self.chemistryCoursesFrame, text = 'CHEM 102', command = lambda: changeChem102(ti))
        if allStudents[ti][6] == 'CHEM 102':
            self.chem102['bg'] = 'light blue'
        self.chem102.pack(side = LEFT)
        self.chem104 = Button(self.chemistryCoursesFrame, text = 'CHEM 104', command = lambda: changeChem104(ti))
        if allStudents[ti][6] == 'CHEM 104':
            self.chem104['bg'] = 'light blue'
        self.chem104.pack(side = LEFT)
        self.chem120 = Button(self.chemistryCoursesFrame, text = 'CHEM 120', command = lambda: changeChem120(ti))
        if allStudents[ti][6] == 'CHEM 120':
            self.chem120['bg'] = 'light blue'
        self.chem120.pack(side = LEFT)
        self.chem222 = Button(self.chemistryCoursesFrame, text = 'CHEM 222', command = lambda: changeChem222(ti))
        if allStudents[ti][6] == 'CHEM 222':
            self.chem222['bg'] = 'light blue'
        self.chem222.pack(side = LEFT)
        self.chem224 = Button(self.chemistryCoursesFrame, text = 'CHEM 224', command = lambda: changeChem224(ti))
        if allStudents[ti][6] == 'CHEM 224':
            self.chem224['bg'] = 'light blue'
        self.chem224.pack(side = LEFT)
        self.chemistryCoursesFrame.pack(fill = X)

        self.physicsCoursesFrame = Frame(self)
        self.phys100 = Button(self.physicsCoursesFrame, text = 'PHYS 100', command = lambda: changePhys100(ti))
        if allStudents[ti][6] == 'PHYS 100':
            self.phys100['bg'] = 'light blue'
        self.phys100.pack(side = LEFT)
        self.phys101 = Button(self.physicsCoursesFrame, text = 'PHYS 101', command = lambda: changePhys101(ti))
        if allStudents[ti][6] == 'PHYS 101':
            self.phys101['bg'] = 'light blue'
        self.phys101.pack(side = LEFT)
        self.phys110 = Button(self.physicsCoursesFrame, text = 'PHYS 110', command = lambda: changePhys110(ti))
        if allStudents[ti][6] == 'PHYS 110':
            self.phys110['bg'] = 'light blue'
        self.phys110.pack(side = LEFT)
        self.phys111 = Button(self.physicsCoursesFrame, text = 'PHYS 111', command = lambda: changePhys111(ti))
        if allStudents[ti][6] == 'PHYS 111':
            self.phys111['bg'] = 'light blue'
        self.phys111.pack(side = LEFT)
        self.phys120 = Button(self.physicsCoursesFrame, text = 'PHYS 120', command = lambda: changePhys120(ti))
        if allStudents[ti][6] == 'PHYS 120':
            self.phys120['bg'] = 'light blue'
        self.phys120.pack(side = LEFT)
        self.phys121 = Button(self.physicsCoursesFrame, text = 'PHYS 121', command = lambda: changePhys121(ti))
        if allStudents[ti][6] == 'PHYS 121':
            self.phys121['bg'] = 'light blue'
        self.phys121.pack(side = LEFT)
        self.otherCourseLabel = Label(self.physicsCoursesFrame)
        self.otherCourseLabel['text' ] = '          Other course (enter full name)'
        self.otherCourseLabel.pack(side = LEFT)
        self.otherCourseInput = Entry(self.physicsCoursesFrame)
        if not (allStudents[ti][6] == 'CHEM 100' or allStudents[ti][6] == 'CHEM 101' or allStudents[ti][6] == 'CHEM 102'
                or allStudents[ti][6] == 'CHEM 104' or allStudents[ti][6] == 'CHEM 120' or allStudents[ti][6] == 'CHEM 222'
                or allStudents[ti][6] == 'CHEM 224' or allStudents[ti][6] == 'PHYS 100' or allStudents[ti][6] == 'PHYS 101'
                or allStudents[ti][6] == 'PHYS 110' or allStudents[ti][6] == 'PHYS 111' or allStudents[ti][6] == 'PHYS 120'
                or allStudents[ti][6] == 'PHYS 121'):
            self.otherCourseInput.insert(0, allStudents[ti][6])
        self.otherCourseInput.pack(side = LEFT, fill = X, expand = YES)
        self.physicsCoursesFrame.pack(fill = X)

        self.buffer4 = Label(self)
        self.buffer4.pack()

        self.tutorFrame = Frame(self)
        self.tutorLabel = Label(self.tutorFrame)
        self.tutorLabel['text'] = 'Tutor'
        self.tutorLabel.pack(side = LEFT)
        self.tutorInput = Entry(self.tutorFrame)
        self.tutorInput.insert(0, allStudents[ti][7])
        self.tutorInput.pack(side = LEFT, fill = X, expand = YES)
        self.tutorFrame.pack(fill = X)

        self.buffer5 = Label(self)
        self.buffer5.pack()

        self.bookFrame = Frame(self)
        self.bookLabel = Label(self.bookFrame)
        self.bookLabel['text'] = 'Book'
        self.bookLabel.pack(side = LEFT)
        self.bookInput = Entry(self.bookFrame)
        self.bookInput.insert(0, allStudents[ti][8])
        self.bookInput.pack(side = LEFT, fill = X, expand = YES)
        self.bookFrame.pack(fill = X)

        self.buffer6 = Label(self)
        self.buffer6.pack()

        self.modStudentFrame = Frame(self)
        self.modStudentButton = Button(self.modStudentFrame,text = 'Done modifying',
                                       bg = 'light green', command = lambda: finalizeMod(ti))
        self.modStudentButton.pack(fill = X, expand = YES, side = BOTTOM)
        self.modStudentFrame.pack(fill = X, expand = YES, side = BOTTOM)

def finalizeMod(ti):
    global fullInfo, allStudents, courseID, majorID, changedMajor, aStudent, changedCourse, headerList, modWindowNotOpen
    timeIn = aStudent.timeInInput.get()
    timeOut = aStudent.timeOutInput.get()
    emailAddress = aStudent.emailInput.get()
    if changedMajor == False:
        major = allStudents[ti][3]
    else:
        major = chooseMajor()
    if aStudent.otherMajorInput.get() != '':
        major = aStudent.otherMajorInput.get()
    changedMajor = False
    lastName = aStudent.lastNameInput.get()
    firstName = aStudent.firstNameInput.get()
    tutorName = aStudent.tutorInput.get()
    bookName = aStudent.bookInput.get()
    if changedCourse == False:
        course = allStudents[ti][6]
    else:
        course = chooseCourse()
    if aStudent.otherCourseInput.get() != '':
        course = aStudent.otherCourseInput.get()
    changedCourse = False
    fullInfo = [timeIn, timeOut, emailAddress, major, lastName, firstName, course, tutorName, bookName,
                allStudents[ti][9], allStudents[ti][10], allStudents[ti][11], allStudents[ti][12], allStudents[ti][13],
                allStudents[ti][14], allStudents[ti][15], allStudents[ti][16], allStudents[ti][17], allStudents[ti][18],
                allStudents[ti][19], allStudents[ti][20], allStudents[ti][21]]
    allStudents[ti][10]['text'] = timeIn
    allStudents[ti][11]['text'] = timeOut
    allStudents[ti][12]['text'] = emailAddress
    allStudents[ti][13]['text'] = major
    allStudents[ti][14]['text'] = lastName
    allStudents[ti][15]['text'] = firstName
    allStudents[ti][16]['text'] = course
    allStudents[ti][17]['text'] = tutorName
    allStudents[ti][18]['text'] = bookName
    allStudents[ti] = fullInfo
    with open ("studentdata.csv", "w") as file:
        file.truncate()
    with open ("studentdata.csv", "a") as file:
        output = csv.writer(file, lineterminator = '\n')
        output.writerow(headerList)
        for key in allStudents:
            output = csv.writer(file, lineterminator = '\n')
            output.writerow(allStudents[key][0:10])
    thirdRoot.destroy()
    modWindowNotOpen = True

def changeBiochemistry(ti):
    global changedMajor, major, majorID, allStudents, aStudent
    aStudent.biochemistryMajor['bg'] = 'light blue'
    majorID = 1
    changedMajor = True
    return

def changeBiology(ti):
    global changedMajor, major, majorID, allStudents, aStudent
    aStudent.biologyMajor['bg'] = 'light blue'
    majorID = 2
    changedMajor = True
    return

def changeChemistry(ti):
    global changedMajor, major, majorID, allStudents, aStudent
    aStudent.chemistryMajor['bg'] = 'light blue'
    majorID = 3
    changedMajor = True
    return

def changeNursing(ti):
    global changedMajor, major, majorID, allStudents, aStudent
    aStudent.nursingMajor['bg'] = 'light blue'
    majorID = 4
    changedMajor = True
    return

def changePb(ti):
    global changedMajor, major, majorID, allStudents, aStudent
    aStudent.pbMajor['bg'] = 'light blue'
    majorID = 5
    changedMajor = True
    return

def changePsychology(ti):
    global changedMajor, major, majorID, allStudents, aStudent
    aStudent.psychologyMajor['bg'] = 'light blue'
    majorID = 6
    changedMajor = True
    return

def changeUndeclared(ti):
    global changedMajor, major, majorID, allStudents, aStudent
    aStudent.undeclaredMajor['bg'] = 'light blue'
    majorID = 7
    changedMajor = True
    return

def changeChem100(ti):
    global changedCourse, course, courseID, allStudents, aStudent
    aStudent.chem100['bg'] = 'light blue'
    courseID = 1
    changedCourse = True
    return

def changeChem101(ti):
    global changedCourse, course, courseID, allStudents, aStudent
    aStudent.chem101['bg'] = 'light blue'
    courseID = 2
    changedCourse = True
    return

def changeChem102(ti):
    global changedCourse, course, courseID, allStudents, aStudent
    aStudent.chem102['bg'] = 'light blue'
    courseID = 3
    changedCourse = True
    return

def changeChem104(ti):
    global changedCourse, course, courseID, allStudents, aStudent
    aStudent.chem104['bg'] = 'light blue'
    courseID = 4
    changedCourse = True
    return

def changeChem120(ti):
    global changedCourse, course, courseID, allStudents, aStudent
    aStudent.chem120['bg'] = 'light blue'
    courseID = 5
    changedCourse = True
    return

def changeChem222(ti):
    global changedCourse, course, courseID, allStudents, aStudent
    aStudent.chem222['bg'] = 'light blue'
    courseID = 6
    changedCourse = True
    return

def changeChem224(ti):
    global changedCourse, course, courseID, allStudents, aStudent
    aStudent.chem224['bg'] = 'light blue'
    courseID = 7
    changedCourse = True
    return

def changePhys100(ti):
    global changedCourse, course, courseID, allStudents, aStudent
    aStudent.phys100['bg'] = 'light blue'
    courseID = 8
    changedCourse = True
    return

def changePhys101(ti):
    global changedCourse, course, courseID, allStudents, aStudent
    aStudent.phys101['bg'] = 'light blue'
    courseID = 9
    changedCourse = True
    return

def changePhys110(ti):
    global changedCourse, course, courseID, allStudents, aStudent
    aStudent.phys110['bg'] = 'light blue'
    courseID = 10
    changedCourse = True
    return

def changePhys111(ti):
    global changedCourse, course, courseID, allStudents, aStudent
    aStudent.phys111['bg'] = 'light blue'
    courseID = 11
    changedCourse = True
    return

def changePhys120(ti):
    global changedCourse, course, courseID, allStudents, aStudent
    aStudent.phys120['bg'] = 'light blue'
    courseID = 12
    changedCourse = True
    return

def changePhys121(ti):
    global changedCourse, course, courseID, allStudents, aStudent
    aStudent.phys121['bg'] = 'light blue'
    courseID = 13
    changedCourse = True
    return 

def chooseMajor():
    global major, majorID
    if majorID == 0:
        major = aStudent.otherMajorInput.get()
        majorID = 0
        return major
    elif majorID == 1:
        major = 'Biochemistry'
        majorID = 0
        return major
    elif majorID == 2:
        major = 'Biology'
        majorID = 0
        return major
    elif majorID == 3:
        major = 'Chemistry'
        majorID = 0
        return major
    elif majorID == 4:
        major = 'Nursing'
        majorID = 0
        return major
    elif majorID == 5:
        major = 'Post-Bacc.'
        majorID = 0
        return major
    elif majorID == 6:
        major = 'Psychology'
        majorID = 0
        return major
    elif majorID == 7:
        major = 'Undeclared'
        majorID = 0
        return major

def pickBiochemistry():
    global majorID
    majorID = 1
    aStudent.biochemistryMajor['bg'] = 'light blue'
def pickBiology():
    global majorID
    majorID = 2
    aStudent.biologyMajor['bg'] = 'light blue'
def pickChemistry():
    global majorID
    majorID = 3
    aStudent.chemistryMajor['bg'] = 'light blue'
def pickNursing():
    global majorID
    majorID = 4
    aStudent.nursingMajor['bg'] = 'light blue'
def pickPb():
    global majorID
    majorID = 5
    aStudent.pbMajor['bg'] = 'light blue'
def pickPsychology():
    global majorID
    majorID = 6
    aStudent.psychologyMajor['bg'] = 'light blue'
def pickUndeclared():
    global majorID
    majorID = 7
    aStudent.undeclaredMajor['bg'] = 'light blue'

def chooseCourse():
    global course, courseID
    if courseID == 0:
        course = aStudent.otherCourseInput.get()
        courseID = 0
        return course
    elif courseID == 1:
        course = 'CHEM 100'
        courseID = 0
        return course
    elif courseID == 2:
        course = 'CHEM 101'
        courseID = 0
        return course
    elif courseID == 3:
        course = 'CHEM 102'
        courseID = 0
        return course
    elif courseID == 4:
        course = 'CHEM 104'
        courseID = 0
        return course
    elif courseID == 5:
        course = 'CHEM 120'
        courseID = 0
        return course
    elif courseID == 6:
        course = 'CHEM 222'
        courseID = 0
        return course
    elif courseID == 7:
        course = 'CHEM 224'
        courseID = 0
        return course
    elif courseID == 8:
        course = 'PHYS 100'
        courseID = 0
        return course
    elif courseID == 9:
        course = 'PHYS 101'
        courseID = 0
        return course
    elif courseID == 10:
        course = 'PHYS 110'
        courseID = 0
        return course
    elif courseID == 11:
        course = 'PHYS 111'
        courseID = 0
        return course
    elif courseID == 12:
        course = 'PHYS 120'
        courseID = 0
        return course
    elif courseID == 13:
        course = 'PHYS 121'
        courseID = 0
        return course

def pickChem100():
    global courseID
    courseID = 1
    aStudent.chem100['bg'] = 'light blue'
def pickChem101():
    global courseID
    courseID = 2
    aStudent.chem101['bg'] = 'light blue'
def pickChem102():
    global courseID
    courseID = 3
    aStudent.chem102['bg'] = 'light blue'
def pickChem104():
    global courseID
    courseID = 4
    aStudent.chem104['bg'] = 'light blue'
def pickChem120():
    global courseID
    courseID = 5
    aStudent.chem120['bg'] = 'light blue'
def pickChem222():
    global courseID
    courseID = 6
    aStudent.chem222['bg'] = 'light blue'
def pickChem224():
    global courseID
    courseID = 7
    aStudent.chem224['bg'] = 'light blue'
def pickPhys100():
    global courseID
    courseID = 8
    aStudent.phys100['bg'] = 'light blue'
def pickPhys101():
    global courseID
    courseID = 9
    aStudent.phys101['bg'] = 'light blue'
def pickPhys110():
    global courseID
    courseID = 10
    aStudent.phys110['bg'] = 'light blue'
def pickPhys111():
    global courseID
    courseID = 11
    aStudent.phys111['bg'] = 'light blue'
def pickPhys120():
    global courseID
    courseID = 12
    aStudent.phys120['bg'] = 'light blue'
def pickPhys121():
    global courseID
    courseID = 13
    aStudent.phys121['bg'] = 'light blue'

def updateTime():
    currentTime = time.asctime(time.localtime(time.time()))
    currentDate['text'] = currentTime
    root.after(1000, updateTime)

def addStudent():
    global fullInfo, allStudents, addWindowNotOpen
    emailAddress = aStudent.emailInput.get()
    major = chooseMajor()
    lastName = aStudent.lastNameInput.get()
    firstName = aStudent.firstNameInput.get()
    tutorName = aStudent.tutorInput.get()
    bookName = aStudent.bookInput.get()
    initials = aStudent.initialInput.get()
    course = chooseCourse()
    currentTime = time.asctime(time.localtime(time.time()))
    timeIn = currentTime
    timeOut = ' '
    fullInfo = [timeIn, timeOut, emailAddress, major, lastName, firstName, course, tutorName, bookName, initials]
    allStudents[timeIn] = fullInfo
    with open("studentdata.csv", "a") as file:
        output = csv.writer(file, lineterminator = '\n')
        output.writerow(fullInfo)
    updateList(fullInfo)
    secondRoot.destroy()
    addWindowNotOpen = True
    return

def closeModWindow():
    return

def modInfo(ti):
    global aStudent, fullInfo, allStudents, thirdRoot, modWindowNotOpen
    if modWindowNotOpen:
        thirdRoot = Tk()
        studentInfoFrame = Frame(thirdRoot)
        aStudent = modWindow(studentInfoFrame, addStudent, ti)
        aStudent.pack(fill = BOTH, expand = YES)
        studentInfoFrame.pack(expand = YES, fill = BOTH)
        modWindowNotOpen = False
        thirdRoot.protocol("WM_DELETE_WINDOW", closeModWindow)

def doTimeOut(ti):
    global aStudent, fullInfo, allStudents
    currentTime = time.asctime(time.localtime(time.time()))
    timeOut = currentTime
    allStudents[ti][1] = timeOut
    allStudents[ti][11]['text'] = timeOut
    with open ("studentdata.csv", "w") as file:
        file.truncate()
    with open ("studentdata.csv", "a") as file:
        output = csv.writer(file, lineterminator = '\n')
        output.writerow(headerList)
        for key in allStudents:
            output = csv.writer(file, lineterminator = '\n')
            output.writerow(allStudents[key][0:10])

def updateList(infoList):
    global xPosition, yPosition, mainCanvas, scrollOne, scrollTwo
    if allStudents[infoList[0]][1] == ' ':
        scrollOne += 30
        scrollTwo += 30
        mainCanvas['scrollregion'] = (0,0,scrollOne,scrollTwo)
        newStudentFrame = Frame(mainCanvas)
        newStudentFrame.configure(width = 1200, height = 20, relief = FLAT)
        newWindow = mainCanvas.create_window(xPosition, yPosition, anchor = NW, window = newStudentFrame)
        timeInLabel = Label(newStudentFrame, width = timeLabelsWidth, bg = 'light blue', text = infoList[0], anchor = W)
        timeOutLabel = Label(newStudentFrame, width = timeLabelsWidth, bg = 'light green', text = infoList[1], anchor = W)
        emailLabel = Label(newStudentFrame, width = emailLabelWidth, bg = 'light blue', text = infoList[2], anchor = W)
        majorLabel = Label(newStudentFrame, width = majorLabelWidth, bg = 'light green', text = infoList[3], anchor = W)
        lastNameLabel = Label(newStudentFrame, width = nameLabelsWidth, bg = 'light blue', text = infoList[4], anchor = W)
        firstNameLabel = Label(newStudentFrame, width = nameLabelsWidth, bg = 'light green', text = infoList[5], anchor = W)
        courseLabel = Label(newStudentFrame, width = courseLabelWidth, bg = 'light blue', text = infoList[6], anchor = W)
        tutorLabel = Label(newStudentFrame, width = tutorLabelWidth, bg = 'light green', text = infoList[7], anchor = W)
        bookLabel = Label(newStudentFrame, width = bookLabelWidth, bg = 'light blue', text = infoList[8], anchor = W)
        modButton = Button(newStudentFrame, text = 'Modify', command = lambda:modInfo(infoList[0]))
        timeOutButton = Button(newStudentFrame, text = 'Time Out', command = lambda:doTimeOut(infoList[0]))
        
        timeInLabel.pack(anchor = NW, side = LEFT)
        timeOutLabel.pack(anchor = NW, side = LEFT)
        emailLabel.pack(anchor = NW, side = LEFT)
        majorLabel.pack(anchor = NW, side = LEFT)
        lastNameLabel.pack(anchor = NW, side = LEFT)
        firstNameLabel.pack(anchor = NW, side = LEFT)
        courseLabel.pack(anchor = NW, side = LEFT)
        tutorLabel.pack(anchor = NW, side = LEFT)
        bookLabel.pack(anchor = NW, side = LEFT)
        modButton.pack(side = RIGHT)
        timeOutButton.pack(side = RIGHT)

        timeInLabel.bind("<MouseWheel>", scrollCanvas)
        timeOutLabel.bind("<MouseWheel>", scrollCanvas)
        emailLabel.bind("<MouseWheel>", scrollCanvas)
        majorLabel.bind("<MouseWheel>", scrollCanvas)
        lastNameLabel.bind("<MouseWheel>", scrollCanvas)
        firstNameLabel.bind("<MouseWheel>", scrollCanvas)
        courseLabel.bind("<MouseWheel>", scrollCanvas)
        tutorLabel.bind("<MouseWheel>", scrollCanvas)
        bookLabel.bind("<MouseWheel>", scrollCanvas)
        modButton.bind("<MouseWheel>", scrollCanvas)
        timeOutButton.bind("<MouseWheel>", scrollCanvas)
        
        allStudents[infoList[0]].append(timeInLabel) #10
        allStudents[infoList[0]].append(timeOutLabel) #11
        allStudents[infoList[0]].append(emailLabel) #12
        allStudents[infoList[0]].append(majorLabel) #13
        allStudents[infoList[0]].append(lastNameLabel) #14
        allStudents[infoList[0]].append(firstNameLabel) #15
        allStudents[infoList[0]].append(courseLabel) #16
        allStudents[infoList[0]].append(tutorLabel) #17
        allStudents[infoList[0]].append(bookLabel) #18
        allStudents[infoList[0]].append(modButton) #19
        allStudents[infoList[0]].append(timeOutButton) #20
        allStudents[infoList[0]].append(newStudentFrame) #21
        yPosition += 30

def closeAddWindow():
    return
        
def addToList():
    global aStudent, secondRoot, addWindowNotOpen
    if addWindowNotOpen:
        secondRoot = Tk()
        studentInfoFrame = Frame(secondRoot)
        aStudent = getInfo(studentInfoFrame, addStudent)
        aStudent.pack(fill = BOTH, expand = YES)
        studentInfoFrame.pack(expand = YES, fill = BOTH)
        addWindowNotOpen = False
        secondRoot.protocol("WM_DELETE_WINDOW", closeAddWindow)

def deleteAnalytics():
    global checkWindowNotOpen
    fourthRoot.destroy()
    checkWindowNotOpen = True

def falseClear():
    global clearWindowNotOpen
    fifthRoot.destroy()
    clearWindowNotOpen = True

def trueClear():
    global yPosition, scrollOne, scrollTwo, allStudents, clearWindowNotOpen
    with open ("SEMESTERSDATA.csv", "a") as file:
        for key in allStudents:
            output = csv.writer(file, lineterminator = '\n')
            output.writerow(allStudents[key][0:10])
    for key in allStudents:
        allStudents[key][21].destroy()
    yPosition = 5
    scrollOne = 0
    scrollTwo = 0    
    allStudents = {}
    fifthRoot.destroy()
    clearWindowNotOpen = True

def closeClearWindow():
    return

def clearCanvas():
    global fifthRoot, clearWindowNotOpen
    if clearWindowNotOpen:
        clearWindowNotOpen = False
        fifthRoot = Tk()
        warningLabel1 = Label(fifthRoot, text = 'Are you absolute sure you want to clear this list?')
        warningLabel1.pack()
        warningLabel2 = Label(fifthRoot, text = 'You will not be able to modify any entries if you do.')
        warningLabel2.pack()
        bottomFrame = Frame(fifthRoot)
        confirmButton = Button(bottomFrame, text = 'OK', command = trueClear)
        confirmButton.pack(side = LEFT)
        rejectButton = Button(bottomFrame, text = 'Cancel', command = falseClear)
        rejectButton.pack(side = RIGHT)
        bottomFrame.pack(expand = YES, fill = X)
        fifthRoot.protocol("WM_DELETE_WINDOW", closeClearWindow)

def closeGetAttendanceWindow():
    return

def getAttendance():
    global fourthRoot, allStudents, checkWindowNotOpen
    if checkWindowNotOpen:
        checkWindowNotOpen = False
        fourthRoot = Tk()
        chem100counter = 0
        chem101counter = 0
        chem102counter = 0
        chem104counter = 0
        chem120counter = 0
        chem222counter = 0
        chem224counter = 0
        phys100counter = 0
        phys101counter = 0
        phys110counter = 0
        phys111counter = 0
        phys120counter = 0
        phys121counter = 0
        otherCounter = 0
        bookCounter = 0
        studentCounter = 0

        for student in allStudents:
            studentCounter += 1
            if allStudents[student][8] != '':
                bookCounter += 1
            if allStudents[student][6] == 'CHEM 100':
                chem100counter += 1
            elif allStudents[student][6] == 'CHEM 101':
                chem101counter += 1
            elif allStudents[student][6] == 'CHEM 102':
                chem102counter += 1
            elif allStudents[student][6] == 'CHEM 104':
                chem104counter += 1
            elif allStudents[student][6] == 'CHEM 120':
                chem120counter += 1
            elif allStudents[student][6] == 'CHEM 222':
                chem222counter += 1
            elif allStudents[student][6] == 'CHEM 224':
                chem224counter += 1
            elif allStudents[student][6] == 'PHYS 100':
                phys100counter += 1
            elif allStudents[student][6] == 'PHYS 101':
                phys101counter += 1
            elif allStudents[student][6] == 'PHYS 110':
                phys110counter += 1
            elif allStudents[student][6] == 'PHYS 111':
                phys111counter += 1
            elif allStudents[student][6] == 'PHYS 120':
                phys120counter += 1
            elif allStudents[student][6] == 'PHYS 121':
                phys121counter += 1
            else:
                otherCounter += 1

        studentlabel = Label(fourthRoot, text = 'Total students for today: ' + str(studentCounter))
        studentlabel.pack()
        chem100label = Label(fourthRoot, text = 'CHEM 100: ' + str(chem100counter))
        chem100label.pack()
        chem101label = Label(fourthRoot, text = 'CHEM 101: ' + str(chem101counter))
        chem101label.pack()
        chem102label = Label(fourthRoot, text = 'CHEM 102: ' + str(chem102counter))
        chem102label.pack()
        chem104label = Label(fourthRoot, text = 'CHEM 104: ' + str(chem104counter))
        chem104label.pack()
        chem120label = Label(fourthRoot, text = 'CHEM 120: ' + str(chem120counter))
        chem120label.pack()
        chem222label = Label(fourthRoot, text = 'CHEM 222: ' + str(chem222counter))
        chem222label.pack()
        chem224label = Label(fourthRoot, text = 'CHEM 224: ' + str(chem224counter))
        chem224label.pack()
        phys100label = Label(fourthRoot, text = 'PHYS 100: ' + str(phys100counter))
        phys100label.pack()
        phys101label = Label(fourthRoot, text = 'PHYS 101: ' + str(phys101counter))
        phys101label.pack()
        phys110label = Label(fourthRoot, text = 'PHYS 110: ' + str(phys110counter))
        phys110label.pack()
        phys111label = Label(fourthRoot, text = 'PHYS 111: ' + str(phys111counter))
        phys111label.pack()
        phys120label = Label(fourthRoot, text = 'PHYS 120: ' + str(phys120counter))
        phys120label.pack()
        phys121label = Label(fourthRoot, text = 'PHYS 121: ' + str(phys121counter))
        phys121label.pack()
        otherLabel = Label(fourthRoot, text = 'Other courses: ' + str(otherCounter))
        otherLabel.pack()
        bookCountLabel = Label(fourthRoot, text = 'Books borrowed: ' + str(bookCounter))
        bookCountLabel.pack()
        killFourthRoot = Button(fourthRoot, text = "         OK                        ", command = deleteAnalytics)
        killFourthRoot.pack(expand = YES, fill = X)
        fourthRoot.protocol("WM_DELETE_WINDOW", closeGetAttendanceWindow)

mainFrame = Frame(root, width = mainFrameWidth, height = mainFrameHeight)
currentTime = time.asctime(time.localtime(time.time()))
currentDate = Label(mainFrame, text = currentTime, bg = 'yellow')
currentDate.pack(anchor = NW)
addOneStudent = Button(mainFrame, text = 'Add student', command = addToList, bg = 'light green')
addOneStudent.pack(anchor = NW)
checkAttendance = Button(mainFrame, text = "Check Today's Analytics", command = getAttendance)
checkAttendance.pack(anchor = NW)
clearAttendance = Button(mainFrame, text = 'Clear Attendance List', command = clearCanvas)
clearAttendance.pack(anchor = NW)
bufferOne = Label(mainFrame)
bufferOne.pack()
todaysList = Label(mainFrame, text = visHeader.format('Time In', 'Time Out', 'E-mail Address', 'Major', 'Last Name', 'First Name', 'Course', 'Tutor', 'Book'))
todaysList.pack(anchor = NW)
mainFrame.grid(row = 0, column = 0)

mainCanvas = Canvas(mainFrame, bg = 'pink', width = mainCanvasWidth, height = mainCanvasHeight, scrollregion = (0,0,scrollOne,scrollTwo))

vbar = Scrollbar(mainFrame, orient = VERTICAL)
vbar.pack(side = RIGHT, fill = Y)
vbar.config(command = mainCanvas.yview)

mainCanvas.config(width = mainCanvasWidth, height = mainCanvasHeight)
mainCanvas.config(yscrollcommand = vbar.set)
mainCanvas.pack(side = LEFT, expand = True, fill = BOTH)

def closeMainWindow():
    if messagebox.askokcancel("Quit", "Are you absolutely sure you want to quit?\nEntries cannot be modified if you quit."):
        with open ("CLOSEDDATA.csv", "a") as file:
            for key in allStudents:
                output = csv.writer(file, lineterminator = '\n')
                output.writerow(allStudents[key][0:10])
        root.destroy()

updateTime()
mainCanvas.bind("<MouseWheel>", scrollCanvas)
root.protocol("WM_DELETE_WINDOW", closeMainWindow)
root.mainloop()
